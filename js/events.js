var Handler = (function (){
	var showContent = function(){
		$('.reveal-content').on('click', function (){
			$(this).find('p').css('display', 'block');

			return false;
		});
	};

	return {
		onReady: function(){
			showContent();
		}
	};
})();

$(document).ready(Handler.onReady());