var gulp = require('gulp');
var browserSync = require('browser-sync');
var gulpSass = require('gulp-sass');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://localhost/envisage/html/"
    });
});

gulp.task('sass', function() {
  return gulp.src('scss/style.scss')
    .pipe(gulpSass({
      sourceComments: 'normal'
    })
      .on('error', gulpSass.logError))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('html', function (){
	return gulp.src('*.html')
			.pipe(browserSync.reload({stream: true}));
});

gulp.task('default', ['sass', 'browser-sync'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['*.html'], ['html']);
});